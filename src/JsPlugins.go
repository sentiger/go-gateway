package src

import (
	"fmt"
	"github.com/robertkrimen/otto"
	"io/ioutil"
	"log"
	"path"
)

func init() {
	plugins := loadPlugins("plugins")
	for _, plugin := range plugins {
		RegisterFilter(plugin.Name, plugin)
	}
}

// 读取内容
func readFile(file string) []byte {
	b, err := ioutil.ReadFile(file)
	if err != nil {
		return []byte("")
	}
	return b
}

type FilterPlugin struct {
	Name  string
	Main  otto.Value
	Order int
}

func (self *FilterPlugin) Filter(exchange *ServerWebExchange) {
	this, _ := otto.ToValue(nil)
	_, err := self.Main.Call(this, exchange) //调用 闭包 ,传递上下文参数
	if err != nil {
		log.Println(err)
	}
}

// 应用
func (self *FilterPlugin) Apply(config interface{}) GatewayFilter {
	return func(exchange *ServerWebExchange) ResponseFilter {
		self.Filter(exchange)
		return nil
	}
}

func (self *FilterPlugin) GetOrder() int {
	fmt.Println(self.Order)
	return self.Order
}

// 初始化插件
func loadPlugin(js *otto.Otto) *FilterPlugin {
	filterName, err := js.Call("name", nil)
	if err != nil {
		log.Println(err)
		return nil
	}
	filterMain, err := js.Call("main", nil)
	if err != nil || !filterMain.IsFunction() {
		log.Println(err)
		return nil
	}

	filterOrder, err := js.Call("order", nil)
	if err != nil || !filterOrder.IsNumber() {
		log.Println(err)
		return nil
	}

	//log.Println(filterName.ToString())
	order, _ := filterOrder.ToInteger()
	return &FilterPlugin{Name: filterName.String(), Main: filterMain, Order: int(order)}
}

// 读取目录中的插件
func loadPlugins(dirname string) []*FilterPlugin {
	ret := make([]*FilterPlugin, 0)
	fileInfos, _ := ioutil.ReadDir(dirname)
	for _, file := range fileInfos {
		if !file.IsDir() && path.Ext(file.Name()) == ".js" {
			js := otto.New()
			_, err := js.Run(readFile(dirname + "/" + file.Name()))
			if err != nil {
				log.Println(err)
				continue
			}
			if p := loadPlugin(js); p != nil {
				ret = append(ret, p)
			}
		}
	}
	return ret

}
