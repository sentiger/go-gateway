package filters

import (
	"github.com/sentiger/go-gateway/src"
)

// 设置代理时候的host
type SetHostFilter struct{}

func init() {
	// 自动注册过滤器
	src.RegisterFilter("SetHost", NewSetHostFilter())
}

func NewSetHostFilter() *SetHostFilter {
	return &SetHostFilter{}
}

func (filter *SetHostFilter) Apply(config interface{}) src.GatewayFilter {
	return func(exchange *src.ServerWebExchange) src.ResponseFilter {
		host := string(src.ValueConfig(config.(string)))
		//host = strings.ReplaceAll(strings.ReplaceAll(host, "https://", ""), "http://", "")
		if host != "" {
			exchange.Request.Host = host
		}
		return nil
	}
}
func (filter *SetHostFilter) GetOrder() int {
	return 2
}
