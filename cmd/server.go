package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
)

type web1Handler struct {
}

func (web1Handler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	writer.Write([]byte("web1 8001 | " + request.URL.Path))
	fmt.Println("8001", request.Header)
}

type web2Handler struct {
}

func (web2Handler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {

	writer.Write([]byte("web2 8002 | " + request.URL.Path))
	fmt.Println("8001", request.Header)
}
func main2() {
	c := make(chan os.Signal)
	go func() {
		http.ListenAndServe(":8001", web1Handler{})
	}()
	go func() {
		http.ListenAndServe(":8002", web2Handler{})
	}()
	signal.Notify(c, os.Interrupt)

	s := <-c
	log.Println(s)
}
